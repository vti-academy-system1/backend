package com.vti.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserController {

	@GetMapping()
	public ResponseEntity<?> getAllTodos(Pageable pageable) {
		return new ResponseEntity<>("Hello world!", HttpStatus.OK);
	}
}
